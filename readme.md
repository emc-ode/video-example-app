## Management
- admin will have ability to:
  - manage videos (upload, edit, delete)
  - manage customers (edit, delete)
  - create another admin user

## CustomerView
- can register to our platform and has Guest role by default
- customer can have different roles:
  - Platinum - has no restriction and can also download
  - Gold - Gold has a limitation to just two or three videos every week/ cannot download
  - Silver - Silver has a limitation to just one video every week/ cannot download 
  - Guest - only can view 2 mins and after 2 mins it should stop 

## Authentication
Authentication will be the same for customer and admin - same page, same form, just different roles.
